batch = open("batch_file.txt", "r")
passports = batch.read().split("\n\n")

print(str(len(passports)) + " Passports found!")

necessaryFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
validHairCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]
validEyeColors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

def isValidFourDigit(data, min, max):
    if not data.isnumeric():
        return False
    return len(data) == 4 and min <= int(data) <= max

def isValidHeight(data):
    if(data.endswith('cm') and data[:-2].isnumeric()):
        return 150 <= int(data[:-2]) <= 193
    if(data.endswith('in') and data[:-2].isnumeric()):
        return 59 <= int(data[:-2]) <= 76
    return False

def isValidHairColor(data):
    if not data.startswith('#'):
        return False
    if len(data[1:]) is not 6:
        return False
    else:
        return all(character in validHairCharacters for character in data[1:])

def ruleCompliant(passportDict):
    byr = passportDict["byr"]
    iyr = passportDict["iyr"]
    eyr = passportDict["eyr"]
    hgt = passportDict["hgt"]
    hcl = passportDict["hcl"]
    ecl = passportDict["ecl"]
    pid = passportDict["pid"]


    byrValid = isValidFourDigit(byr, 1920, 2002)
    iyrValid = isValidFourDigit(iyr, 2010, 2020)
    eyrValid = isValidFourDigit(eyr, 2020, 2030)
    hgtValid = isValidHeight(hgt) 
    hclValid = isValidHairColor(hcl)
    eclValid = ecl in validEyeColors
    pidValid = pid.isnumeric() and len(pid) == 9

    return byrValid and iyrValid and eyrValid and hgtValid and hclValid and eclValid and pidValid

amountValid = 0
for passport in passports:
    list = passport.split()
    fields = [element.split(':')[0] for element in list]
    values = [element.split(':')[1] for element in list]
   
    
    amountValid += all(necessaryField in fields for necessaryField in necessaryFields) and ruleCompliant(dict(zip(fields, values)))
        
    #print(dict(zip(fields, values))) 

print("Valid passports: " + str(amountValid))