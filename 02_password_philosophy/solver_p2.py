# get the passwords
expense_report = open("passwords.txt", "r")
lines = expense_report.read().split("\n")

def password_valid(password_line):
    splitted = password_line.split(" ")
    range = splitted[0].split("-")
    min = int(range[0])
    max = int(range [1])
    
    letter_needed = splitted[1][0]

    password = splitted[2]

    return (password[min-1] == letter_needed) != (password[max-1] == letter_needed) #xor


sum = 0
for password in lines:
    if(password_valid(password)):
        sum += 1

print("The amount of valid passwords are: " + str(sum))

