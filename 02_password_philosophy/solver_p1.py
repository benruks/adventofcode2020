# get the passwords
expense_report = open("passwords.txt", "r")
lines = expense_report.read().split("\n")

def password_valid(password_line):
    splitted = password_line.split(" ")
    range = splitted[0].split("-")
    min = int(range[0])
    max = int(range [1])
    
    letter_needed = splitted[1][0]

    password = splitted[2]

    occurencies = 0
    for letter in password:
        if(letter == letter_needed):
            occurencies +=1

    if(occurencies >= min and occurencies <= max):
        return True
    else:
        return False


sum = 0
for password in lines:
    if(password_valid(password)):
        sum += 1

print("The amount of valid passwords are: " + str(sum))

