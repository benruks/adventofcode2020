batch = open("boarding_passes.txt", "r")
passes = batch.read().split("\n")

def getPassId(data):
    min = 0
    max = 128
    for letter in data[:7]:
        if(letter == 'F'):
            max -= int((max - min) / 2)
        else:
            min += int((max - min) / 2)

    row = min

    min = 0
    max = 8
    for letter in data[-3:]:
        if(letter == 'L'):
            max -= int((max - min) / 2)
        else:
            min += int((max - min) / 2)
    
    column = min

    return row * 8 + column
    

passIds = []
for boardingPass in passes:
    passIds.append(getPassId(boardingPass))
    

missingIds = []
for i in range(max(passIds)):
    if i not in passIds:
        missingIds.append(i)

print("The highest boarding pass ID is: " + str(max(passIds)))

print("Missin ID's:")
print(missingIds)

