map = open("map.txt", "r")
lines = map.read().split("\n")

print("there are " + str(len(lines)) + " lines and " + str(len(lines[0])) + " rows in this map")

moves = [   {"rightStep": 1, "downStep": 1},
            {"rightStep": 3, "downStep": 1},
            {"rightStep": 5, "downStep": 1},
            {"rightStep": 7, "downStep": 1},
            {"rightStep": 1, "downStep": 2}
]

answer = 1

for move in moves:
    rightStep = move["rightStep"]
    downStep = move["downStep"]
    sumTrees = 0
    for i in  range(downStep, len(lines), downStep):
        if(lines[i][(int(i/downStep)*rightStep)%len(lines[i])] == '#'):
            sumTrees += 1
    answer *= sumTrees
    print("Encountered trees for (right: " + str(rightStep) + ", down " + str(downStep) + "): " + str(sumTrees))

print("multiplied together, these produce the answer: " + str(answer))