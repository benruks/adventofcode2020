# get the numbers
expense_report = open("expense_report.txt", "r")
lines = expense_report.read().split("\n")

# convert to integers
lines = [int(x) for x in lines]

# find and print the solution
for expense_1 in lines:
    for expense_2 in lines:
        if(expense_1 + expense_2 == 2020):
            print("The Solution is: " + str(expense_1 * expense_2))
            exit()
