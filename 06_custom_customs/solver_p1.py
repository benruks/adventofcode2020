data = open("answers.txt", "r")
group_answers = data.read().split("\n\n")

for i in range(len(group_answers)):
    group_answers[i] = group_answers[i].split('\n')


def getSumYes(data):
    questionYes = []
    for personAnswer in data:
        for question in personAnswer:
            if question not in questionYes:
                questionYes.append(question)

    return len(questionYes)
        

sumOfAllGroupsYes = 0
for group_answer in group_answers:
    sumOfAllGroupsYes += getSumYes(group_answer)

print("Sum of yes count: " + str(sumOfAllGroupsYes))
